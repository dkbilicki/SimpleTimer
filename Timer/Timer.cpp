#include "Timer.h"

#include <future>

/**
 * Default constructor.
 */
Timer::Timer() {
}

/**
 * Constructor. Starts timer with given configuration.
 */
Timer::Timer(TimeAmount delay, Event event, TimeAmount period) {
	start(delay, event, period);
}

/**
 * Destructor.
 */
Timer::~Timer() {
	// Stop timer and wait for it to finish.
	stop();
	wait();
}

/**
 * Activity flag getter.
 */
bool Timer::isActive() {
	return mActive;
}

/**
 * Activity flag setter.
 */
void Timer::setActive(bool value) {
	std::unique_lock<std::mutex> lock(mMutex);
	mActive = value;
}

/**
 * Start timer.
 */
void Timer::start(TimeAmount delay, Event event, TimeAmount period) {
	// Validate input data - negative values are forbidden.
	if (delay.count() < 0 || period.count() < 0) {
		throw std::invalid_argument("Negative value passed as delay or period!");
	}

	// If timer is running, stop it and wait.
	stop();
	wait();
	
	// Set timer to active and run new thread.
	setActive(true);
	mThreadPtr.reset(new std::thread(&Timer::run, this, delay, event, period));
}

/**
 * Stop timer.
 */
void Timer::stop() {
	// Clear activity flag, notify timer thread to exit wait condition if necessary and wait for it to finish.
	setActive(false);
	mConditionVariable.notify_one();
}

/**
 * Wait for timer to finish.
 */
void Timer::wait() {
	// Join timer's thread and wait for it to finish.
	if (mThreadPtr.get() != nullptr && mThreadPtr->joinable()) {
		mThreadPtr->join();
	}
}

/**
 * Timer's delay/periodic wait method.
 */
void Timer::timerWait(const TimePoint unlockTime) {
	// If active, wait until given time-point or exit when timer is stopped (condition variable notification).
	std::unique_lock<std::mutex> lock(mMutex);
	mConditionVariable.wait_until(lock, unlockTime, [this] { return !isActive(); });
}

/**
 * Timer's main loop function.
 */
void Timer::run(TimeAmount delay, Event event, TimeAmount period) {
	// For first iteration use current time and delay to set unlock time.
	TimePoint unlockTime = std::chrono::steady_clock::now() + delay;

	// Check if timer is periodic.
	bool isPeriodic = period.count() > 0;

	// One pass if one-shot, repeat if periodic and active.
	do {
		// Wait to given time-point.
		timerWait(unlockTime);

		// Check if timer is still active after waiting.
		if (isActive()) {
			// If timer is active, fire event.
			event();

			// Set next unlock time using previous one and given period.
			unlockTime += period;
		}
	} while (isActive() && isPeriodic);

	// If one-shot and not stopped earlier, clear activity flag.
	setActive(false);
}