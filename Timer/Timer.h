#pragma once

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>

/**
 * Simple one-shot/periodic timer.
 *
 * Class can be used as one-shot (period parameter = 0) or periodic timer.
 * Default time unit is millisecond, but class is implemented using std::chrono, 
 * so any time unit from this library larger than ms can be used as TimeAmount.
 * Timer automatically stops and waits for its thread to finish when object goes out of scope.
 */
class Timer
{
public:
	typedef std::chrono::milliseconds TimeAmount; /**< Milliseconds - basic time unit for timer. */
	typedef const std::function<void()> Event; /**< Event to be fired by timer (functor). */
	typedef std::unique_ptr<std::thread> ThreadPtr; /**< Thread pointer. */

protected:
	typedef std::chrono::steady_clock::time_point TimePoint; /**< Time point - chrono::steady_clock. */

	bool mActive = false; /**< Timer activity flag.*/

	std::mutex mMutex; /**< Mutex for timer's thread synchronization. */
	std::condition_variable mConditionVariable; /**< Condition variable for timer's thread synchronization. */
	ThreadPtr mThreadPtr; /**< Timer thread handler. */

	/**
	 * Activity flag setter.
	 * @param value	a new value for activity flag.
	 */
	void setActive(bool value);

	/**
	 * Timer's delay/periodic wait method.
	 * @param value	a new value for activity flag.
	 */
	void timerWait(const TimePoint unlockTime);

	/**
	 * Timer's main loop function.
	 * @param delay		an amount of time until timer starts.
	 * @param event		a functor that will be fired as timer event.
	 * @param period	duration of one cycle - enables periodic timer.
	 */
	void run(TimeAmount delay, Event event, TimeAmount period);

public:
	/**
	 * Default constructor.
	 */
	Timer();

	/**
	 * Constructor. Starts timer with given configuration.
	 * @param delay		an amount of time until timer starts.
	 * @param event		a functor that will be fired as timer event.
	 * @param period	duration of one cycle - enables periodic timer.
	 * @see start()
	 */
	Timer(TimeAmount delay, Event event, TimeAmount period = TimeAmount(0));

	/**
	 * Destructor.
	 */
	virtual ~Timer();

	/**
	 * Deleted copy constructor.
	 */
	Timer(const Timer&) = delete;

	/**
	 * Deleted assignment operator.
	 */
	Timer& operator=(const Timer&) = delete;

	/**
	 * Activity flag getter.
	 * @return The current value of activity flag.
	 */
	bool isActive();

	/**
	 * Start timer.
	 * @param delay		an amount of time until timer starts.
	 * @param event		a functor that will be fired as timer event.
	 * @param period	duration of one cycle - enables periodic timer.
	 */
	void start(TimeAmount delay, Event event, TimeAmount period = TimeAmount(0));

	/**
	 * Stop timer.
	 */
	void stop();

	/**
	 * Wait for timer to finish.
	 * NOTE: If timer is set to periodic mode and no stop condition is given, it will run infinitely.
	 */
	void wait();
};

