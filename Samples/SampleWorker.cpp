#include "SampleWorker.h"

/**
 * Constructor.
 */
SampleWorker::SampleWorker() {
}

/**
 * Constructor with start work function.
 */
SampleWorker::SampleWorker(Timer::TimeAmount delay, Timer::TimeAmount period, Timer::Event event) {
	startWork(delay, period, event);
}

/**
 * Destructor.
 */
SampleWorker::~SampleWorker() {
}

/**
 * Start worker.
 */
void SampleWorker::startWork(Timer::TimeAmount delay, Timer::TimeAmount period, Timer::Event event) {
	timer.start(delay, event, period);
}

/**
 * Stop worker.
 */
void SampleWorker::stopWork() {
	timer.stop();
}