#include "SampleStopWatch.h"

/**
 * Constructor.
 */
SampleStopWatch::SampleStopWatch() {
	reset();
}

/**
 * Destructor.
 */
SampleStopWatch::~SampleStopWatch() {
}

/**
 * Get time passed.
 */
Timer::TimeAmount SampleStopWatch::getTime() {
	return mTimePassed;
}

/**
 * Start stop-watch.
 */
void SampleStopWatch::start() {
	reset();
	// Start timer without delay and increment counted time every 1 ms.
	mTimer.start(std::chrono::seconds(0), [this] { ++mTimePassed; }, std::chrono::milliseconds(1));
}

/**
 * Stop stop-watch.
 */
void SampleStopWatch::stop() {
	mTimer.stop();
}

/**
 * Reset stop-watch.
 */
void SampleStopWatch::reset() {
	mTimePassed = std::chrono::milliseconds(0);
}