#pragma once

#include <Timer.h>

/**
 * Very simple synchronic worker class.
 */
class SampleWorker
{
private:
	Timer timer; /**< Timer used by worker. */
public:
	/**
	 * Constructor.
	 */
	SampleWorker();

	/**
	 * Constructor with start work function.
	 * @see startWork()
	 * @param delay		an amount of time until worker starts.
	 * @param period	duration of one wait cycle.
	 * @param event		a functor that will called by worker.
	 */
	SampleWorker(Timer::TimeAmount delay, Timer::TimeAmount period, Timer::Event event);

	/**
	 * Destructor.
	 */
	virtual ~SampleWorker();

	/**
	 * Start worker.
	 * @param delay		an amount of time until worker starts.
	 * @param period	duration of one wait cycle.
	 * @param event		a functor that will called by worker.
	 */
	void startWork(Timer::TimeAmount delay, Timer::TimeAmount period, Timer::Event event);

	/**
	 * Stop worker.
	 */
	void stopWork();
};

