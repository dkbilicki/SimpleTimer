#pragma once

#include <Timer.h>

/**
 * Very simple stop-watch class.
 */
class SampleStopWatch
{
private:
	Timer mTimer; /**< Timer used by stop-watch. */
	std::chrono::milliseconds mTimePassed; /**< Time counted by stop-watch (in ms) */
public:
	/**
	 * Constructor.
	 */
	SampleStopWatch();
	
	/**
	 * Destructor.
	 */
	virtual ~SampleStopWatch();

	/**
	 * Start stop-watch.
	 */
	void start();
	
	/**
	 * Stop stop-watch.
	 */
	void stop();
	
	/**
	 * Get time passed.
	 * @return Counted time.
	 */
	std::chrono::milliseconds getTime();

	/**
	 * Reset stop-watch.
	 */
	void reset();
};

