#include <chrono>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <random>
#include <sstream>
#include <thread>

#include "SampleStopWatch.h"
#include "SampleWorker.h"

/**
 * Stop-watch sample.
 *
 * Sample will generate random time from range 1-5000 ms and will start the stop-watch.
 * After given time it will stop the stop-watch and print time it registered.
 */
void sampleStopWatch() {
	std::cout << "=== 1. Stop-watch ===" << std::endl;

	SampleStopWatch stopWatch;

	// Initialize random generator.
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist(1, 5000);
	
	// Set waiting time at random.
	int waitTime = dist(rng);
	std::cout << "Stop after " << waitTime << " milliseconds." << std::endl;

	// Start stop-watch.
	stopWatch.start();

	// Do wait...
	std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));

	// Stop and get time measurement.
	stopWatch.stop();
	auto timePassed = stopWatch.getTime().count();
	
	std::cout
		<< "Time passed: "
		<< std::setfill('0')
		<< (timePassed / 60000) /* minutes */
		<< ":"
		<< std::setw(2)
		<< (timePassed / 1000) % 60 /* seconds */
		<< ":"
		<< std::setw(3)
		<< (timePassed % 1000) /* milliseconds */
		<< std::endl;
}

/**
 * Producer-consumer sample.
 *
 * Sample will start producer and consumer threads. They are synchronized via mutex.
 * Every time producer puts new value to the buffer, it will print it.
 * Consumer runs more frequent than producer. Every time it sees new value in the buffer, it will print it
 * and clear the buffer.
 */
void sampleProducerConsumer() {
	std::cout << "=== 2. Producer-consumer ===" << std::endl;

	// Initialize random generator.
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> generator(1, 10);

	// "Buffer" for producer and consument.
	int value = 0;

	// Mutex for threads synchronization.
	std::mutex mutex;

	// Start producer with delay.
	SampleWorker producer(
		std::chrono::milliseconds(500),
		std::chrono::seconds(1),
		[&value, &mutex, &generator, &rng] { 
			std::unique_lock<std::mutex> lock(mutex);
			value = generator(rng); 
			std::cout << "<- Produced: " << value << std::endl;  
		});

	// Start consumer.
	SampleWorker consumer(
		std::chrono::seconds(0),
		std::chrono::milliseconds(333),
		[&value, &mutex] {
			std::unique_lock<std::mutex> lock(mutex);
			if (value) { 
				std::cout << "-> Consumed: " << value << std::endl;
				value = 0;
			} else {
				std::cout << "Nothing to consume..." << std::endl;
			}
		});

	// Wait for a while while producer and consumer are working.
	std::this_thread::sleep_for(std::chrono::seconds(5));
}

/**
 * Data validator sample.
 *
 * Sample sets timer for timeout and waits for random amount of time - longer or 
 * shorter than timeout value.
 * If timeout was reached, it will print information about data being invalid.
 * If not, it will print that data is still valid.
 */
void sampleDataValidator() {
	std::cout << "=== 3. Data validator ===" << std::endl;

	// Initialize random generator.
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> generator(500, 1500); /**< Generates value between 500-1500 (milliseconds). */

	Timer timeoutValidator;
	auto timeout = std::chrono::seconds(1); /**< Set timeout to 1 second. */
	std::atomic_bool isValid = false;

	std::cout << "Timeout after " << timeout.count() << " second(s)." << std::endl;

	// Try running timeout check for a few times.
	for (int i = 0; i < 5; i++) {
		isValid = true;
		timeoutValidator.start(timeout, [&isValid] { isValid = false; });
		
		int waitTime = generator(rng);
		std::cout << "Will wait for " << waitTime << " milliseconds." << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));

		std::cout << "Data " << (isValid ? " still valid." : " invalid.") << std::endl;
	}

	timeoutValidator.stop();
}

int main(int argc, char* argv[]) {
	bool invalid = false;

	// Only one input parameter allowed.
	if (argc != 2) {
		std::cerr << "Invalid number of arguments!" << std::endl;
		return 1;
	}

	std::stringstream ss;
	ss << argv[1];
	int sampleNr;

	if ((ss >> sampleNr).fail()) {
		invalid = true;
	} else {
		// Select sample based on input parameters.
		switch (sampleNr) {
		case 1:
			sampleStopWatch();
			break;
		case 2:
			sampleProducerConsumer();
			break;
		case 3:
			sampleDataValidator();
			break;
		default:
			invalid = true;
		}
	}

	// If argument was invalid, return error.
	if (invalid) {
		std::cerr << "Invalid argument!" << std::endl;
		return 2;
	}

	return 0;
}