#include "stdafx.h"
#include "CppUnitTest.h"

#include <Timer.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TimerTest
{
	TEST_CLASS(TimerInitTests)
	{
	public:
		TEST_METHOD(Init) {
			// Check constructor/destructor behavior.
			Timer timer;
		}

		TEST_METHOD(InitCheckActive) {
			// Check activity flag initialization.
			Timer timer;
			Assert::IsFalse(timer.isActive(), L"Timer activity flag is set.");
		}

		TEST_METHOD(InitStop) {
			// Check stop() behavior after initialization.
			Timer timer;
			timer.stop();
			timer.stop();
		}

		TEST_METHOD(InitWait) {
			// Check wait() behavior after initialization.
			Timer timer;
			timer.wait();
			timer.wait();
		}

		void setFlagAfterTime(Timer& timer, int ms, bool assert) {
			timer.start(Timer::TimeAmount(ms), [assert]() mutable { if (assert) Assert::Fail(L"Timeout."); });
		}

		TEST_METHOD(Restart) {
			// Check restart while running.
			Timer timer;
			std::atomic_bool t1Finished = false, t2Finished = false, t3Finished = false;

			setFlagAfterTime(timer, 1000, true);
			setFlagAfterTime(timer, 1000, true);
			setFlagAfterTime(timer, 1000, false);
			timer.wait();
		}

		TEST_METHOD(StartStop) {
			// Check multiple start/stop cycles.
			Timer timer;
			timer.start(Timer::TimeAmount(1000), [] { Assert::Fail(L"Timeout 1"); }, Timer::TimeAmount(1000));
			Assert::IsTrue(timer.isActive(), L"Timer didn't started: 1.");
			timer.stop();
			Assert::IsFalse(timer.isActive(), L"Timer didn't stopped: 1.");
			timer.start(Timer::TimeAmount(1000), [] { Assert::Fail(L"Timeout 2"); }, Timer::TimeAmount(1000));
			Assert::IsTrue(timer.isActive(), L"Timer didn't started: 2.");
			timer.stop();
			Assert::IsFalse(timer.isActive(), L"Timer didn't stopped: 2.");
		}

		bool checkInvalidArgumentException(Timer& timer, int duration, int period) {
			try {
				timer.start(Timer::TimeAmount(duration), [] {}, Timer::TimeAmount(period));
			}
			catch (std::invalid_argument) {
				return true;
			}
			catch (...) {
			}

			return false;
		}

		TEST_METHOD(CheckInputExceptions) {
			// Check bad params exceptions.
			Timer timer;

			Assert::IsTrue(checkInvalidArgumentException(timer, -10, 0), L"No exception for param. duration < 0.");
			Assert::IsTrue(checkInvalidArgumentException(timer, 0, -10), L"No exception for param. period < 0.");
			Assert::IsTrue(checkInvalidArgumentException(timer, -10, -10), L"No exception for param. duration < 0 and period < 0.");
		}

		TEST_METHOD(OneShotStartWait) {
			// Check standard one-shot timer.
			Timer timer;
			std::atomic_bool timerFired = false;

			timer.start(Timer::TimeAmount(1000), [&timerFired] { timerFired = true; });
			Assert::IsTrue(timer.isActive(), L"Timer didn't started.");

			timer.wait();

			Assert::IsFalse(timer.isActive(), L"Timer didn't stopped.");
			Assert::IsTrue(timerFired.load(), L"Timer didn't fired event.");
		}

		TEST_METHOD(ChronoUnits) {
			// Check units conversions.
			Timer timer;
			std::atomic_bool timerFired = false;

			timer.start(std::chrono::seconds(1), [&timerFired] { timerFired = true; });
			Assert::IsTrue(timer.isActive(), L"Timer didn't started.");

			timer.wait();

			Assert::IsFalse(timer.isActive(), L"Timer didn't stopped.");
			Assert::IsTrue(timerFired.load(), L"Timer didn't fired event.");
		}

		TEST_METHOD(PeriodicStartWait) {
			// Check standard periodic timer.
			Timer timer;
			std::atomic_bool timerFired = false;
			std::atomic_int timerCounter = 0;

			auto timerFunc = [&timer, &timerFired, &timerCounter] {
				if (timerCounter++ >= 10) {
					timerFired = true;
					timer.stop();
				}
			};

			timer.start(Timer::TimeAmount(0), timerFunc, Timer::TimeAmount(100));
			Assert::IsTrue(timer.isActive(), L"Timer didn't started.");

			timer.wait();

			Assert::IsFalse(timer.isActive(), L"Timer didn't stopped.");
			Assert::IsTrue(timerFired.load(), L"Timer didn't fired event.");
		}

		TEST_METHOD(Synchronize) {
			// Check thread synchronization using timers.
			Timer timer1;
			Timer timer2;

			timer1.start(Timer::TimeAmount(0), []() {}, Timer::TimeAmount(100));
			timer2.start(Timer::TimeAmount(1000), [&timer1] { timer1.stop(); });

			Assert::IsTrue(timer1.isActive(), L"Timer 1 didn't started.");
			Assert::IsTrue(timer2.isActive(), L"Timer 2 didn't started.");

			timer1.wait();

			Assert::IsFalse(timer1.isActive(), L"Timer 1 didn't stopped.");
			Assert::IsFalse(timer2.isActive(), L"Timer 2 didn't stopped.");
		}
	};
}