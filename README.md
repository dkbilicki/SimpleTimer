# Simple timer library

VS 2015 project - simple timer implementation. Timer class can be used as one-shot or periodic timer. Default time unit is millisecond, but class is implemented using std::chrono, so any time unit from this library larger than millisecond can be used as TimeAmount. Timer automatically stops and waits for its thread to finish when object goes out of scope.

## Samples

To run samples you either need to build project and run it through console (syntax Samples.exe [sampleNr]) or set Samples as a startup project in VS and add command line parameter through project properties.

Available samples:

### 1. Stop-watch

Start stop-watch, wait for random period of time (up to 5s), then stop stop-watch and print counted time.

### 2. Producer-consumer

Very simple synchronic producer-consumer scenario - producer puts some value to a buffer (random integer) and prints it, consumer checks buffer frequently and when it sees new value, it prints it and resets buffer.

### 3. Data validator

Validation check - if data processing took too much time (timeout) its result can be ignored.